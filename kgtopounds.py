class KgToPounds:
    def __init__(self, kg):
        self.kg = kg

    def to_pounds(self):
        return self.kg * 2.205

    def set_kg(self, kg_x):
        self.kg = kg_x

    def get_kg(self):
        return self.kg
a=KgToPounds(10)
print(a.to_pounds())
